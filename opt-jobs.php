<?php
/* Personalizacion del plugin conforme a los requerimientos
@author Aris Mota <aris.mota@opt-media.net>
@version 1.0
@date 2019-08-01
*/
$optjobs["etapa"] = array(
  'cv_recibido'     => __('CV Recibido', 'opt-jobs-board'),
  'cv_corregido'     => __('CV Corregido', 'opt-jobs-board'),
  'test_personalidad'     => __('Test de personalidad', 'opt-jobs-board'),
  'entrevista_skype'     => __('Entrevista Skype', 'opt-jobs-board'),
  'entrevista_personal'     => __('Entrevista Personal', 'opt-jobs-board'),
  'seleccionado'     => __('Seleccionado', 'opt-jobs-board')
);
